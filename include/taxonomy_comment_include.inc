<?php

/**
 * @file
 * Comments listing function and bulk operations for the module.
 */

/**
 * Implementing Comments Listing form.
 */
function taxonomy_comment_listing($form, &$form_state) {
  $header = array(
    'subject' => t('Subject'),
    'author' => t('Author'),
    'posted_in' => t('Posted In'),
    'status' => t('Status'),
    'updated' => t('Updated'),
    'operations' => t('Operations'),
  );
  $result = db_select('taxonomy_comment', 'tc')
    ->extend('PagerDefault')
    ->limit(50)
    ->fields('tc')
    ->execute();
  $rows = array();
  while ($commentdata = $result->fetchAssoc()) {
    $comment_user = taxonomy_comment_userload($commentdata['uid']);
    $comment_term = taxonomy_comment_termload($commentdata['tid']);
    $commentstatus[] = $commentdata['status'];
    $rows[$commentdata['t_cid']] = array(
      'subject' => check_plain($commentdata['tc_title']),
      'author' => check_plain($comment_user),
      'posted_in' => check_plain($comment_term),
      'status' => ($commentdata['status'] == 0) ? t('Unpublished') : t('Published'),
      'updated' => date('d/m/y H:i:a', $commentdata['changed']),
      'operations' => l(t('Edit'), 'taxonomy_comment/' . $commentdata['t_cid'] . '/edit'),
    );
  }
  if (isset($commentstatus) && !empty($commentstatus) && is_array($commentstatus)) {
    if (in_array(0, $commentstatus) && in_array(1, $commentstatus)) {
      $options = array(
        'publish' => t('Publish selected Comment'),
        'unpublish' => t('Unpublish selected Comment'),
        'delete' => t('Delete selected Comment'),
      );
    }
    elseif (in_array(1, $commentstatus)) {
      $options = array(
        'unpublish' => t('Unpublish selected Comment'),
        'delete' => t('Delete selected Comment'),
      );
    }
    elseif (in_array(0, $commentstatus)) {
      $options = array(
        'publish' => t('Publish selected Comment'),
        'delete' => t('Delete selected Comment'),
      );
    }
  }
  else {
    $options = array('0' => t('No Action'));
  }
  // Implements comment listing form.
  if (!isset($form_state['storage']['confirm'])) {
    $form['action_select'] = array(
      '#type' => 'select',
      '#title' => t('CHOOSE ANY OPTION'),
      '#options' => $options,
      '#empty_value' => '',
      '#description' => t('This selection will perform your action'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );
    $form['list_comment'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => t('No Comments Found'),
    );
    $form['pager'] = array(
      '#theme' => 'pager',
    );

    return $form;
  }
  else {
    // Implemets confirm, for bulk delete operations.
    if ($form_state['values']['action_select'] == 'delete') {
      $content = '<h4>' . t('Do you  want to remove comment') . '</h4>';
      $form['intro'] = array(
        '#markup' => $content,
        '#prefix' => '<div class= "commnet-delete">',
        '#suffix' => '</div>',
      );
      $message = t('Are you sure you want to delete?');
      $caption = t('This action cannot be undone.');
      return confirm_form($form, $message, 'admin/content/taxonomy-comment', $caption, t('Delete'));
    }
    // Implemets confirm, for bulk update status operations.
    elseif ($form_state['values']['action_select'] == 'publish' || $form_state['values']['action_select'] == 'unpublish') {
      $content = '<h4>' . t('Do you  want to update comment status') . '</h4>';
      $form['intro'] = array(
        '#markup' => $content,
        '#prefix' => '<div class= "commnet-update">',
        '#suffix' => '</div>',
      );
      $message = t('Are you sure you want to update?');
      $caption = t('This action cannot be undone.');
      return confirm_form($form, $message, 'admin/content/taxonomy-comment', $caption, t('Update'));
    }
  }
}

/**
 * Returns the comments author name.
 */
function taxonomy_comment_userload($uid) {
  $user_name = db_query("SELECT name from {users} WHERE uid = :uid ", array(":uid" => $uid))->fetchField();
  return $user_name;
}

/**
 * Returns the commented term name.
 */
function taxonomy_comment_termload($tid) {
  $term_name = db_query("SELECT name from {taxonomy_term_data} WHERE tid = :tid ", array(":tid" => $tid))->fetchField();
  return $term_name;
}

/**
 * Implements form listing submit.
 */
function taxonomy_comment_listing_submit($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = TRUE;
    $form_state['storage']['values'] = $form_state['values'];
    $form_state['rebuild'] = TRUE;
  }
  else {
    $action = $form_state['storage']['values']['action_select'];
    if ($action == 'delete') {
      $ids = $form_state['storage']['values']['list_comment'];
      foreach ($ids as $value) {
        if ($value == 0) {
          unset($value);
        }
        else {
          $values[] = $value;
        }
      }
      if (isset($values) && !empty($values)) {
        $result = db_select('taxonomy_comment', 'tc')
          ->fields('tc', array('t_cid'))
          ->condition('t_cid', $values, 'IN')
          ->execute()
          ->fetchAll();
        $operations = array();
        $operations[] = array('trim', array(''));
        $count = 1;
        $last_row = count($result);
        foreach ($result as $row) {
          $commentids[] = $row->t_cid;
          if ($count === $last_row) {
            $operations[] = db_delete('taxonomy_comment')
              ->condition('t_cid', $commentids, 'IN')
              ->execute();
            $commentids = array();
          }
          ++$count;
        }
        $batch = array(
          'operations' => $operations,
          'finished' => 'taxonomy_comment_bulkdelete_batch_finished',
          'title' => t('Deleting !datacount Taxonomy Comments.', array(
            '!datacount' => --$count,
          )),
        );
        batch_set($batch);
        batch_process();
      }
    }
    elseif ($action == 'unpublish' || $action == 'publish') {
      $actionstatus = $form_state['storage']['values']['action_select'];
      if ($actionstatus == 'unpublish') {
        $updatestatus = 0;
      }
      elseif ($actionstatus == 'publish') {
        $updatestatus = 1;
      }
      $ids = $form_state['storage']['values']['list_comment'];
      foreach ($ids as $value) {
        if ($value == 0) {
          unset($value);
        }
        else {
          $values[] = $value;
        }
      }
      if (isset($values) && !empty($values)) {
        $result = db_select('taxonomy_comment', 'tc')
          ->fields('tc', array('t_cid'))
          ->condition('t_cid', $values, 'IN')
          ->execute()
          ->fetchAll();
        $operations = array();
        $operations[] = array('trim', array(''));
        $count = 1;
        $last_row = count($result);
        foreach ($result as $row) {
          $commentids[] = $row->t_cid;
          if ($count % 20 === 0 || $count === $last_row) {
            $operations[] = db_update('taxonomy_comment')
              ->fields(array(
                'status' => $updatestatus,
              ))
              ->condition('t_cid', $values, 'IN')
              ->execute();
            $commentids = array();
          }
          ++$count;
        }
        $batch = array(
          'operations' => $operations,
          'finished' => 'taxonomy_comment_bulkupdate_batch_finished',
          'title' => t('Updating !datacount Taxonomy Comments', array(
            '!datacount' => --$count,
          )),
        );
        batch_set($batch);
        batch_process();
      }
    }
  }
}

/**
 * Implements delete confirm for each comment delete.
 */
function taxonomy_comment_del_confirm($form, &$form_state, $id) {
  $current_tid = db_query("SELECT tid from {taxonomy_comment} WHERE t_cid = :t_cid ", array(":t_cid" => $id))->fetchField();
  $form['comment_id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  return confirm_form(
      $form, t('Are you sure you want to delete this?'), 'taxonomy/term/' . $current_tid, t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

/**
 * Implements delete confirm for delete operation.
 */
function taxonomy_comment_del_confirm_submit($form, &$form_state) {
  $c_cid = $form_state['values']['comment_id'];
  $currenttid = db_query("SELECT tid from {taxonomy_comment} WHERE t_cid = :t_cid ", array(":t_cid" => $c_cid))->fetchField();
  db_delete('taxonomy_comment')
    ->condition('t_cid', $c_cid, '=')
    ->execute();
  drupal_set_message(t('Comment has been deleted!'));
  $form_state['redirect'] = "taxonomy/term/" . $currenttid;
}

/**
 * Implements edit operation for a comment.
 */
function taxonomy_comment_edit() {
  $updateid = arg(1);
  $commentid = db_query("SELECT * from {taxonomy_comment} WHERE t_cid = :t_cid ", array(":t_cid" => $updateid))->fetchField();
  return drupal_get_form('taxonomy_comment_form', $commentid);
}
