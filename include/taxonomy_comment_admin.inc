<?php

/**
 * @file
 * Taxonomy Comments vocabulary configuration to enable/disable comments.
 */

/**
 * Implementing the vocabulary configuration form.
 */
function taxonomy_comment_vocab_form($form, &$form_state) {
  $query = db_select('taxonomy_vocabulary', 'tax_vocab');
  $result = $query
    ->fields('tax_vocab', array('name', 'vid', 'machine_name'))
    ->execute();
  foreach ($result as $row) {
    $vocablists[$row->vid] = ucwords($row->name);
  }
  $form['taxonomy_comment_vocab'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select Vocabulary'),
    '#description' => t('Comment will be enabled for seleted vocabulary terms'),
    '#options' => $vocablists,
    '#default_value' => variable_get('taxonomy_comment_vocab', array()),
  );
  $form = system_settings_form($form);
  return $form;
}
