-- SUMMARY --

By enabling this module commenting will be enable for a taxonomy term.

-- REQUIREMENTS --

You need to set some permission for text formates:

If there are multiple roles on your site you need to give the text format
permission for FULL HTML to those roles who are responsible to make a comment
by visiting admin/people/permissions/roles 
 

Step: visit admin/people/permissions/roles and edit permissions link will make 
this configuration complete

-- INSTALLATION --

By Downloading this module follow the steps:

1. Extract this to your sites/all/modules folder
2. By installing this visit to admin/structure/taxonomy/comment_vocab and choose 
   a vocabulary to make the comment form enable for that vocabulary
3. If you have configured the 2nd step you will see a comment form for each term 
   of that taxonomy
4. You can see all the comment lists for the taxonomy terms by visiting 
   here admin/content/taxonomy-comment
5. Set the permission admin/people/permissions for 
    a. Edit Own Comments
    b. Delete Own Comments
    c. List all the comments to perform bulk operations
