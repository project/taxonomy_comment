<?php

/**
 * @file
 * Comments template file that would list each comments on it's terms page.
 */
/**
 * Load the comments for the current taxonomy term.
 */
?>
<div id="taxonomy-comments-wrap">
<?php $count = count($output); ?>
  <?php if ($count > 0) : ?> 
    <span class="block-heading"><?php print t("Comments"); ?></span>
  <?php endif; ?>
<?php foreach ($output as $key => $c_row): ?>
  <?php if ($key == $count - 1) : ?>
    <div id="taxonomy-comments-nob">
  <?php else: ?>
    <div id="taxonomy-comments-wb">
  <?php endif; ?>
  <div class="c-author-details">
    <span class="auth-name">
      <?php
      global $base_url;
      print t("Author Name: <a href='@author-link'>@author</a>",
      array(
        '@author' => $author[$c_row->uid],
        '@author-link' => $base_url . '/user/' . $c_row->uid,
      )); ?>
    </span>
    <span class="created-date"><?php print t("Posted On:") . date('D/M/Y H:i:a', $c_row->created); ?></span>
  </div>
  <div class="comment-details">
      <span class="comment-title"><h3><?php print $c_row->tc_title; ?></h3></span>
      <span class="comment-body"><?php print $c_row->tc_body; ?></span>
      <span class="edit-comment"><?php print (isset($op_edit[$c_row->t_cid])) ? $op_edit[$c_row->t_cid] : ''; ?></span>
      <span class="delete-comment"><?php print (isset($op_del[$c_row->t_cid])) ? $op_del[$c_row->t_cid] : ''; ?></span>
  </div>
</div>
<?php endforeach; ?>
</div>
</div>
